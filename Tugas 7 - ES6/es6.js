var line ='======================\n';
console.log(line + 'No. 1. Mengubah fungsi menjadi fungsi arrow \n' + line);

const golden = ()=>{
  console.log("this is golden!!");
}

golden();






var line ='======================\n';
console.log('\n\n'+ line + 'No. 2. Sederhanakan menjadi Object literal di ES6\n' + line);

const newFunction = (firstName, lastName) => {
  return { 
  	firstName,
  	lastName,
    fullName(){
      console.log(`${firstName} ${lastName}`);
    }
  }
}
 

//Driver Code 
newFunction("William", "Imoh").fullName();  
console.log(newFunction("Wiltesliam", "Imoh").firstName); 










var line ='======================\n';
console.log('\n\n'+ line + 'No. 3. Destructuring\n' + line);

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}


const {firstName, lastName, destination, occupation, spell} = newObject;

// Driver code
console.log(firstName, lastName, destination, occupation);








 
var line ='======================\n';
console.log('\n\n'+ line + 'No. 4. Array Spreading\n' + line);




const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];
//Driver Code
console.log(combined);






var line ='======================\n';
console.log('\n\n'+ line + 'No. 5. Template Literals\n' + line);


const planet = "earth";
const view = "glass";
var before = `Lorem ${view}dolor sit amet, consectetur adipiscing elit, ${planet}do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`;
 
// Driver Code
console.log(before);


