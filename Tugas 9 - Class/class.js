var line ='======================\n';
console.log('\n\n'+ line + 'No. 1 Animal Class - Release 0\n' + line);

class Animal {
     constructor(name, legs=4, cold_blooded=false) {
        this._name = name;
        this._legs = legs;
        this._cold_blooded = cold_blooded;
    }

    get name(){
    	return this._name;	
    }

    get legs(){
    	return this._legs;	
    }

    get cold_blooded(){
    	return this._cold_blooded;	
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false



var line ='======================\n';
console.log('\n\n'+ line + 'No. 1 Animal Class - Release 1\n' + line);

// Code class Ape dan class Frog di sini
 
class Ape extends Animal{
  constructor(name, legs) {
    super(name,legs);
  }
  yell() {
    return console.log("Auooo");
  }
} 

var sungokong = new Ape("kera sakti", 2);
// console.log(sungokong.name);
// console.log(sungokong.legs);
// console.log(sungokong.cold_blooded);
sungokong.yell() // "Auooo"
 
class Frog extends Animal{
 	constructor(name){
		super(name);
	}
  jump() {
    return console.log("hop hop");
  }
}

var kodok = new Frog("buduk");
// console.log(kodok.name);
// console.log(kodok.legs); 
// console.log(kodok.cold_blooded);
kodok.jump(); // "hop hop" 



console.log('\n\n'+ line + 'No. 2 Function to Class\n' + line);


class Clock {
        
  constructor({template}) {
    this.template = template;
  }

  render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = this.template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  stop(){
    clearInterval(this.timer);
  };

  start(){
    this.render();
    this.timer = setInterval(()=>this.render(), 1000);
  };
};

var clock = new Clock({template: 'h:m:s'});
clock.start(); 

