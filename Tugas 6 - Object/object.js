var line ='======================\n';
console.log(line + 'No. 1 (Array to Object) \n' + line);

var now = new Date();
var thisYear = now.getFullYear(); // 2020 (tahun sekarang)

function arrayToObject(arr) {
    var ret = {}; 
    for (var i = 0 ; i <= arr.length-1; i++) {

      var name = `${i+1}. ${arr[i][0]} ${arr[i][1]}`; 
      var itm = {};
      itm.firstName = arr[i][0];
      itm.lastName = arr[i][1];
      itm.gender = arr[i][2];
      var age = thisYear - arr[i][3];
      if (arr[i][3] == undefined || age < 0){
         age = "Invalid Birth Year";
      } 
      itm.age = age;
       ret[name]=itm;

    }
  console.log(ret);
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
arrayToObject(people); 
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ];
arrayToObject(people2);
 
// Error case 
arrayToObject([]); // ""


var line ='======================\n';
console.log('\n\n'+ line + 'No. 2 (Shopping Time)\n' + line);


function shoppingTime(memberId='', money='') {
   var tokoX = [
     {
       barang: 'Sepatu Stacattu',
       harga: 1500000,
     },

     {
       barang: 'Baju Zoro',
       harga: 500000,
     },

     {
       barang: 'Baju H&N',
       harga: 250000,
     },

     {
       barang: 'Sweater Uniklooh',
       harga: 175000,
     },

     {
       barang: 'Casing Handphone',
       harga: 50000,
     },

   ];
   var ret =  ''
   if(memberId == ''){
    ret +=  'Mohon maaf, toko X hanya berlaku untuk member saja.';
   } else if (memberId != '' && money < 50000){
    ret +=  'Mohon maaf, uang tidak cukup!';
   } else { 
   var ret = {};
   ret.memberId = memberId;
   ret.money = money;
   var buy = []; 
   for (var i = 0; i <= tokoX.length -1; i++){
       if(money >= tokoX[i].harga){
         money -= tokoX[i].harga;
         buy.push(tokoX[i].barang);
       }
   }
   
   ret.listPurchased = buy;
   ret.changeMoney = money;
   }
   return ret;
}
 
 
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000)); 
console.log(shoppingTime('82Ku8Ma742', 170000)); 
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja



var line ='======================\n';
console.log('\n\n'+ line + 'No. 3 (Naik Angkot)\n' + line);

function naikAngkot(arr) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  var ret = [];
  for(var i = 0; i <= arr.length -1; i++){
     var itm = {}; 
      itm.penumpang = arr[i][0];
      itm.naikDari = arr[i][1];
      itm.tujuan = arr[i][2];
      var pay = 0;
      for(j=rute.indexOf(arr[i][1]); j <= rute.indexOf(arr[i][2])-1 ; j++){
          pay+=2000;
      }
      itm.bayar = pay;
      ret.push(itm);
  }

  
  return ret;
  }
 
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]


 

