var line ='======================\n';
console.log(line + 'No. 1 (Range) \n' + line);

function range(startNum, finishNum) {
  var x = -1;
  if(startNum != '' && startNum != finishNum){ 
    if(typeof startNum == 'number' && typeof finishNum == 'number'){
    	var x = [];
       if(startNum < finishNum){
       	 	while(startNum <= finishNum){
       	 		x.push(startNum);
       	 		startNum++;
       	 	}
       } else {
       		while(startNum >= finishNum){
       			x.push(startNum);
       			startNum--;
       		}
       }  

    }  
  }  

  return x;
 }
 
console.log(range(1, 10)); 
console.log(range(1));
console.log(range(11,18)); 
console.log(range(54, 50)); 
console.log(range());



var line ='======================\n';
console.log('\n\n'+ line + 'No. 2 (Range with Step)\n' + line);


function rangeWithStep(startNum, finishNum, step=1) {
  var x = -1;
  if(startNum != '' && startNum != finishNum){ 
    if(typeof startNum == 'number' && typeof finishNum == 'number' && typeof step == 'number'){
    	var x = [];
       if(startNum < finishNum){
       	 	while(startNum <= finishNum){
       	 		x.push(startNum);
       	 		startNum+=step;
       	 	}
       } else {
       		while(startNum >= finishNum){
       			x.push(startNum);
       			startNum-=step;
       		}
       }  

    }  
  }  
  
  return x;
 }
 
 
console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5] 
console.log(rangeWithStep(1, 10)); 



var line ='======================\n';
console.log('\n\n'+ line + 'No. 3 (Sum of Range)\n' + line);

function sum(startNum, finishNum=0, step=1) {
  var x = 0;
  if(startNum != '' && startNum != finishNum){ 
    if(typeof startNum == 'number' && typeof finishNum == 'number' && typeof step == 'number'){
       if(startNum < finishNum){
       	 	while(startNum <= finishNum){
       	 		x += startNum;
       	 		startNum+=step;
       	 	}
       } else {
       		while(startNum >= finishNum){ 
       	 		x += startNum;
       			startNum-=step;
       		}
       }  

    }  
  }  
  
  return x;
 }

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 




var line ='======================\n';
console.log('\n\n'+ line + 'No. 4 (Array Multidimensi)\n' + line);

//contoh input
var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ] ;

function dataHandling(data) { 
	if(Array.isArray(data)){ 
	  var print = '';
      for (var i = 0; i < data.length; i++) { 
      	print += `Nomor ID:  ${data[i][0]}\nNama Lengkap: ${data[i][1]}\nTTL: ${data[i][2]}, ${data[i][3]}\nHobi: ${data[i][4]}\n`;
      	print += '\n';
      }
      return print;
	} else {
	   return 'Input not array!';
	}
}


console.log(dataHandling(input)); 



var line ='======================\n';
console.log('\n\n'+ line + 'No. 5 (Balik Kata)\n' + line);




function balikKata(inp) {
  var out = 'is not string';
  if (typeof inp == 'string') {
  	   var out = '';
	   for (var i = inp.length-1; i >= 0; i--) {
	     out += inp[i];
	   }
  }  

  return out;
}

console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I  




var line ='======================\n';
console.log('\n\n'+ line + 'No. 6 (Metode Array)\n' + line);


var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
var fruits = [ "banana", "orange", "grape"]

function dataHandling2(inp){
	inp.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung"); 
	inp.splice(4, 0, "Pria", "SMA Internasional Metro"); 
	console.log(inp);

	let splitCase = inp[3].split('/');  
	var bulan = parseInt(splitCase[1]);
	switch(bulan) {
		  case 1:   { bulan = "Januari"; break; }
		  case 2:   { bulan = "Februari"; break; }
		  case 3:   { bulan = "Maret"; break; }
		  case 4:   { bulan = "April";break; }
		  case 5:   { bulan = "Mei";break; }
		  case 6:   { bulan = "Juni";break; }
		  case 7:   { bulan = "Juli";break; }
		  case 8:   { bulan = "Agustus";break; }
		  case 9:   { bulan = "September";break; }
		  case 10:   { bulan = "Oktober";break; }
		  case 11:   { bulan = "November";break; }
		  case 12:   { bulan = "Desember";break; }
		  default:   { bulan = "Januari";break; } 
		}
	console.log(bulan);

	var sortCase = splitCase.sort(function (value1, value2) { return value2 - value1 } ) ;
	console.log(sortCase); // [12, 3, 1] 

	var splitCase2 = inp[3].split('/');
	var joinCase = splitCase2.join('-');
	console.log(joinCase);

    var sliceCase = inp[1].slice(0,14);
    console.log(sliceCase);

}


dataHandling2(input); 
  

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 


