//======================
// No.. 1 Looping while
//======================

var line ='======================\n';
console.log(line + 'No. 1 Looping while\n' + line);

//code:

var test=2;
console.log('LOPPING PERTAMA');
while(test < 21) {  
  console.log(`${test} - I love coding`);  
  test+=2; 
}
console.log('LOPPING KEDUA');

while(test > 2 ) {  
  test-=2; 
  console.log(`${test} - I will become a mobile developer`);  
}

//============
// No..2 Looping menggunakan for
//============

console.log('\n\n'+ line + 'No.2 Looping menggunakan for\n' + line);

for(var angka = 1; angka < 21; angka++) {
  if ((angka%2) != 0 && (angka%3) == 0){
  console.log(`${angka} - I Love Coding`);
  } else if ((angka%2) == 0){
  console.log(`${angka} - Berkualitas`);
  } else {
  console.log(`${angka} - Santai`);
  } 
} 

//============
// No. 3 Membuat persegi panjang #
//============

console.log('\n\n'+ line + 'No. 3 Membuat persegi panjang #\n' + line);

var r = 4;
var c = 8;
var output = '';


console.log('for Looping\n');

for (let i = r; i > 0; i--) {
	for (let j = c; j > 0; j--) {
      output += '#';
   }	
   output += '\n' ;
}

console.log(output);

console.log('while Looping\n');

var output = '';
while(r > 0){
	 var c2 = c;
	 while (c2 > 0){
	    output += '#';
	  	c2--;
	 }
	 output +='\n';
	 r--;
}

console.log(output);

//============
// No. 4 Membuat Tangga 
//============

console.log('\n\n'+ line + 'No. 4 Membuat Tangga \n' + line);

var s = 7;
var output ='';


console.log('for Looping\n');
for (var i = 0; i < s; i++) {
	for (var j = 0; j <= i; j++) {
	  output += '#';
	}
    output+='\n';
}

console.log(output); 

console.log('while Looping\n');
var output ='';
var i = 0;
while (i<s){
	var j = 0;
	while(j<=i){
	  output += '#';
	  j++	
	}
	output += '\n';
	i++;
}

console.log(output); 



//============
// No. 5 Membuat Papan Catur 
//============

console.log('\n\n'+ line + 'No. 5 Membuat Papan Catur \n' + line);

var s = 8;
var out ='';
for (var i = 1; i <= s; i++) {
	for (var j = 1; j <= s ; j++) {
       if((i%2) == 0){
          if((j%2) == 0){
          	out += ' ';
          }else{
          	out += '#';
          }
       } else{
          if((j%2) == 0){
          	out += '#';
          }else{
          	out += ' ';
          }
       }
	}
	out += '\n';
}

console.log(out); 

function myApp(){
    var total = 5;
    var output = "";
    for (var i=1; i<=total; i++){
        for (var j=1; j<=i; j++){
            output += j + " ";
        }
        console.log(output);
        output="";
    }
}

myApp();

 
