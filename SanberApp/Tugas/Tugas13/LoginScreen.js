import React, {Component} from 'react' ;
import {StyleSheet, View, Text, Image, TouchableHighlight, Button, TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons'; 


 function MyInput({ title }) {
  return (
    <View>
       <Text>{title}</Text> 
       <TextInput  
        style={styles.inputText} 
      />
    </View>
  )
}
 



export default class App extends Component {
	render(){
		return (
			<View style={styles.container}>  
	       			<Image style={styles.imageLogo} source={require('./images/logo.png')}/> 
     				<Text style={styles.title}>Register</Text> 
     				<View style={{paddingBottom: 30}}>
     				<MyInput title="Username"/>
     				<MyInput title="Email"/>
     				<MyInput title="Password"/>
     				<MyInput title="Ulangi Password" />
     				</View>
     				
     				
     				<TouchableHighlight>
			            <Text style = {styles.button1}>
			               Daftar
			            </Text>
			        </TouchableHighlight>
     				<Text style={{paddingVertical: 10}}>Atau</Text> 
     				<TouchableHighlight>
			            <Text style = {styles.button2}>
			               Masuk
			            </Text>
			        </TouchableHighlight>

			</View>

		)
	}
}


const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		margin: 40,
		backgroundColor: 'white', 
	},


	imageLogo:{ 
      	marginBottom: 20,  
      	width: 300,
      	height: 100
	}, 

	title: { 
		color: '#003366',
		fontSize: 30,
		padding: 10,
		marginBottom: 0
	},
    inputText:{
    	//flexDirection: 'row',
    	//alignItems: 'stretch',
    	fontSize: 30,  
    	borderColor: 'grey',
    	borderWidth: 1,
    	borderRadius: 10,
    	width: 250,
    	marginBottom: 5,
    	paddingHorizontal: 20,
    	paddingVertical: 5,
    	fontSize: 20
    },

	button1:{ 
		fontSize: 20,
		borderWidth: 1,
		paddingHorizontal: 25,
		paddingVertical: 5, 
		borderColor: '#003366',
		color: 'white',
		backgroundColor: '#003366',
		borderRadius: 10
	},
	button2:{ 
		fontSize: 20,
		borderWidth: 1,
		paddingHorizontal: 25,
		paddingVertical: 5, 
		borderColor: '#003366',
		color: '#3EC6FF',
		backgroundColor: 'white',
		borderRadius: 10
	},
 
})