import React, {Component} from 'react' ;
import {StyleSheet, View, Text, Image, TouchableHighlight, TouchableOpacity, Button, TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome'; 


 function MyInput({ title }) {
  return (
    <View>
       <Text>{title}</Text> 
       <TextInput  
        style={styles.inputText} 
      />
    </View>
  )
}
 



export default class App extends Component {
	render(){
		return (
			<View style={styles.container}>  
     				<Text style={styles.title}>Tentang Saya</Text>  
			        <View style={styles.imageLogo} >
	       				<Image source={require('./images/about.png')} style={{width: 100, height: 100}}/> 
     				</View>
     				<Text style={styles.title}>Sherlock Diee</Text>  
     				<Text style={styles.title}>React Native Developer</Text>  
     				
			        <View style={styles.box} >
     				 	<Text style={styles.title2}>Portofolio</Text>  
     				 	<View style={styles.iconIcon}> 
				          <TouchableOpacity>
				            <Icon name="github" size={50}/> 
				          </TouchableOpacity>
				          <TouchableOpacity>
				            <Icon name="gitlab" size={50} color={'#d25518'}/> 
				          </TouchableOpacity>
     				 	</View>
			        </View>

			          <View style={styles.box} >
     				 	<Text style={styles.title2}>Contact</Text>  
     				 	<View style={styles.iconIcon}> 
				          <TouchableOpacity >
				            <Icon name="facebook-square" size={50} color={'#1759ae'}/> 
				          </TouchableOpacity>
				          <TouchableOpacity>
				            <Icon name="twitter" size={50} color={'#188b91'}/> 
				          </TouchableOpacity>
				          <TouchableOpacity>
				            <Icon name="instagram" size={50} color={'#ce193b'}/> 
				          </TouchableOpacity>
     				 	</View>
			        </View>

			</View>

		)
	}
}


const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		margin: 40,
		backgroundColor: 'white', 
	},


	imageLogo:{   
      	width: 130,
      	height: 130,
      	backgroundColor: '#EFEFEF',
      	borderRadius: 60, 
      	alignItems: 'center',
      	flexDirection: 'column',
		justifyContent: 'center',
		marginVertical: 15

	}, 

	title: { 
		color: '#003366',
		fontSize: 20,
		paddingBottom: 10, 
	},
	box:{
		width: 300,
		backgroundColor: '#EFEFEF',
		padding: 10,
		marginBottom: 20,
		borderRadius: 10
	},
	title2:{
		fontSize: 20,
		borderBottomWidth: 1
	},
	iconIcon:{
		flexDirection: 'row',
		alignItems: 'center', 
		justifyContent: 'space-around',
		paddingVertical: 10
	}

 
})