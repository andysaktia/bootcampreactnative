import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { NavigationContainer } from "@react-navigation/native";

import LoginScreen from "./LoginScreen";
import AboutScreen from "./AboutScreen";
import AddScreen from "./AddScreen";
import OtherScreen from "./OtherScreen";
import ProjectScreen from "./ProjectScreen";
import SkillScreen from "./SkillScreen";

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const navigation = ()=>( 
		<NavigationContainer>
			<Stack.Navigator
				 screenOptions={{ headerShown:false }}
				>
				<Stack.Screen name="LoginScreen" component={LoginScreen}/>
				<Stack.Screen name="DrawerScreen" component={DrawerScreen}/>
			</Stack.Navigator>
		</NavigationContainer> 
)


const DrawerScreen = ()=>( 
		<Drawer.Navigator>
			<Drawer.Screen name="MainApp" component={MainApp}/>
			<Drawer.Screen name="AboutScreen" component={AboutScreen}/>
			<Drawer.Screen name="OtherScreen" component={OtherScreen}/>
		</Drawer.Navigator>  
)


const MainApp = ()=>( 
		<Tab.Navigator>
			<Tab.Screen name="SkillScreen" component={SkillScreen}/>
			<Tab.Screen name="ProjectScreen" component={ProjectScreen}/>
			<Tab.Screen name="AddScreen" component={AddScreen}/>
		</Tab.Navigator>  
)
 

 export default navigation;