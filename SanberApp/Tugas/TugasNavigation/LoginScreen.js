import React from 'react'
import { StyleSheet, Text, Touchable, TouchableOpacity, View, Button } from 'react-native'


export default function LoginScreen ({navigation}){
    return (
        <View style={styles.container}>
            <Text>Login Screen</Text>
             <Button
		      title="Menuju Skill Screen"
		      onPress={() =>
		        navigation.navigate("DrawerScreen")
		      }
		    /> 
        </View>
    )
}




const styles = StyleSheet.create({
	container:{
		flex: 1,
		alignItems: 'center',
		justifyContent:'center',
		padding: 20
	},
})
