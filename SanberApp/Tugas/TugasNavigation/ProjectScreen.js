import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

export default function ProjectScreen (){
    return (
        <View style={styles.container}> 
            <Text>Halmaan project screen</Text>
        </View>
    )
}




const styles = StyleSheet.create({
	container:{
		flex: 1,
		alignItems: 'center',
		justifyContent:'center',
		padding: 20
	},
})
