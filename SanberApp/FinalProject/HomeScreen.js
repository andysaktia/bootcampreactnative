import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  Button,
} from 'react-native';
 

const DEVICE = Dimensions.get('window');

export default class HomeScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      nameProv: '', 
      dataApi: [],
    };
  }

 
  componentDidMount(){
    this.fetchData();
  }

  fetchData = async()=>{
    const response = await fetch("https://api.kawalcorona.com/indonesia/provinsi/");
    const json = await response.json();
    this.setState({ dataApi: json})
  }

   




  render() { 
    return ( 
      <View style={styles.container}>  
        <View
          style={{
            minHeight: 50,
            width: DEVICE.width * 0.88 + 20,
            marginVertical: 8,
          }}
        >
          <View
            style={{ flexDirection: 'row', justifyContent: 'space-between' }}
          >
            <Text>
              Hai,{'\n'}
               <Text style={styles.headerText}>
                {this.props.route.params.userName}
              </Text>
            </Text>

             <Text style={{ textAlign: 'right' }}> 
              <Text style={styles.headerText}>
                <Button onPress={()=>this.props.navigation.navigate('Profile')} title="Profile"/>
              </Text>
            </Text>
          </View>
          <View></View> 

 
        </View>
 
        <FlatList
        data={this.state.dataApi}
        renderItem={(item)=>(

        <TouchableOpacity 
             onPress={()=>this.props.navigation.navigate('Detail')}
          >
          <ListItem 
            data={item}
            updatePrice={()=>
              this.updatePrice(produk.item.harga)}
            />
        </TouchableOpacity>
          )}
        keyExtractor={(x, i)=> i}
        numColumns={2}
        />

      </View>
    );
  }
}






class ListItem extends React.Component {  
  render() {
    const data = this.props.data.item.attributes;
    return (
      <View style={styles.itemContainer}> 
        <Text numberOfLines={2} ellipsizeMode="tail" style={styles.itemName}>
          {data.Provinsi}
        </Text>
        <Text style={styles.itemPrice}>
          Positif: {data.Kasus_Posi}
        </Text>
        <Text style={styles.itemStock}>
          Sembuh: {data.Kasus_Semb}
        </Text>
        <Text style={styles.itemStock}>
          Meninggal: {data.Kasus_Meni}
        </Text> 
      </View>
    );
  }
}





const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold',
  },

  //? Lanjutkan styling di sini
  itemContainer: {
    width: DEVICE.width * 0.44,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#FFFFFF',
    marginHorizontal: 5,
    marginVertical: 5,
    padding: 5,
  },
  itemImage: {
    height:100,
    width:100,
    resizeMode:"contain"
  },
  itemName: {
    fontWeight: 'bold'
  },
  itemPrice: {
    fontWeight: 'bold',
    color:'red'
  },
  itemStock: {
    fontWeight: 'bold'
  },
  itemButton: {

  },
  buttonText: {
    
  },

});
