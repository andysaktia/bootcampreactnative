import * as React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';  

import LoginScreen from './LoginScreen';
import HomeScreen from './HomeScreen';
import ProfileScreen from './Profile';
import DetailScreen from './Detail';
import SplashScreen from './Splash';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

/*
export default class App extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Login" >
                <Stack.Screen name="Splash" component={SplashScreen}/>
                <Stack.Screen name='Login' component={LoginScreen} options={{ headerShown: false }} />
                <Stack.Screen name='Home' component={HomeScreen} options={{ headerTitle: 'Info Covid' }} />
                <Stack.Screen name="Profile" component={ProfileScreen} /> 
                <Stack.Screen name="Detail" component={DetailScreen} /> 

                <Stack.Screen name="MainApp" component={MainApp} />
                <Stack.Screen name="MyDrawwer" component={MyDrawwer}/>
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

 */

const index = () => {
    return (
        <NavigationContainer >
            <Stack.Navigator initialRouteName='Login'>
                <Stack.Screen name="Splash" component={SplashScreen}/>
                <Stack.Screen name="Login" component={LoginScreen}  options={{ headerShown: false }}/>
                <Stack.Screen name="Home" component={HomeScreen} options={{ headerTitle: 'Info Covid' }}/>
                <Stack.Screen name="Profile" component={ProfileScreen} /> 
                <Stack.Screen name="Detail" component={DetailScreen} /> 

                <Stack.Screen name="MainApp" component={MainApp} />
                <Stack.Screen name="MyDrawwer" component={MyDrawwer}/>
            </Stack.Navigator>
      </NavigationContainer>
    )
}


const MainApp =() =>{
    return(
      <Tab.Navigator>
        <Tab.Screen name="Login" component={LoginScreen} />
        <Tab.Screen name="Profile" component={ProfileScreen} />
      </Tab.Navigator>
    )
}
const MyDrawwer = ()=>{
    return(
    <Drawer.Navigator initialRouteName="Home"> 
        <Drawer.Screen name="Login" component={LoginScreen} />
        <Drawer.Screen name="Profile" component={ProfileScreen} />
    </Drawer.Navigator>
    )
}

export default index;
 
