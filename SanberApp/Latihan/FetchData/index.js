
import React from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';  


export default class App extends React.Component{
	state = {
		data:[]
	}

	componentDidMount(){
		this.fetchData();
	}
 	fetchData = async()=>{
 		const response = await fetch("https://api.kawalcorona.com/indonesia/provinsi/");
 		const json = await response.json();
 		this.setState({ data: json})
 	}

	render(){
		return (
			<View style={styles.container}>
			   <FlatList data = {this.state.data}
			   keyExtractor={(x, i)=> i}
			   renderItem={({item})=> (
			   <Text>{item.attributes.Provinsi}</Text>
			)}/>
			</View>
		);
	}
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50, 
  },
});