var readBooksPromise = require('./promise.js');
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
];
 

 
const getPromise= async(t, arr)=>{
    for(let i=0; i<arr.length; i++){
      t = await readBooksPromise(t, arr[i])
        .then(function(sisaWaktu){
           return sisaWaktu;
        })
        .catch(function(sisaWaktu){
           return sisaWaktu;  
        })
    } 
}


getPromise(10000, books); 