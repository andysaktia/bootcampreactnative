 
//============
// Soal if-else
//============
var nama = "John";
var peran = "Guard";  // isi dengan penyihir/guard/werewolf

if (nama == '' && peran == ''){
  console.log("Nama harus diisi!");
} else if( nama != '' && peran == ''){
  console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`);

} else {
  console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
  if (peran == 'Penyihir'){ 
  		console.log(`Halo Penyihir ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`)
  } else if (peran == 'Guard'){
  		console.log(`Halo Guard ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.`)
  } else if (peran == 'Werewolf'){
  		console.log(`Halo Werewolf ${nama}, Kamu akan memakan mangsa setiap malam!`)
  } else { 
  		console.log(`Halo ${nama}, peran ${peran} tidak ada, peran yang tersedia Penyihir, Guard, dan Werewolf`)
  }

}  



//============
// Soal Switch Case
//============

var tanggal = 17; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 8; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1945; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)


switch(bulan) {
  case 1:   { bulan = "Januari"; break; }
  case 2:   { bulan = "Februari"; break; }
  case 3:   { bulan = "Maret"; break; }
  case 4:   { bulan = "April";break; }
  case 5:   { bulan = "Mei";break; }
  case 6:   { bulan = "Juni";break; }
  case 7:   { bulan = "Juli";break; }
  case 8:   { bulan = "Agustus";break; }
  case 9:   { bulan = "September";break; }
  case 10:   { bulan = "Oktober";break; }
  case 11:   { bulan = "November";break; }
  case 12:   { bulan = "Desember";break; }
  default:   { bulan = "Januari";break; } 
}

// crosscheck tanggal
if (tanggal > 31 || tanggal < 1){
	if (tanggal > 31){ tanggal = 31}
	if (tanggal < 1){tanggal = 1}
	console.log('tanggal harus di antara 1-31')
}


var format = tanggal + ' ' + bulan + ' ' + tahun; 

console.log(format);

 