
//============
// Soal No.1
//============
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

// Output yang diharapkan
// JavaScript is awesome and I love it! 

// With Concat 
var op1 = word.concat(' ').concat(second).concat(' ').concat(third).concat(' ').concat(fourth).concat(' ').concat(fifth).concat(' ').concat(sixth).concat(' ').concat(seventh);

// With Operator and string spasi
var op2 = word + ' ' + second + ' ' + third + ' ' + fourth + ' ' + fifth + ' ' + sixth + ' ' + seventh;
// Test OutPut
//console.log(op2);
 
 


//============
// Soal No.2
//============
var s = "I am going to be React Native Developer"; 

var firstWord = s[0] ; 
var secondWord = s[2] + s[3]; 
var thirdWord = s[5] + s[6] + s[7] + s[8] + s[9];
var fourthWord = s[11] + s[12] ;
var fifthWord = s[14] + s[15] ;
var sixthWord = s[17] + s[18] + s[19] + s[20] + s[21];
var seventhWord = s[23] + s[24] +  s[25] + s[26] + s[27] + s[28];
var eighthWord = s[30] + s[31] + s[32] + s[33] + s[34] + s[35] + s[36] + s[37] + s[38];

// console.log('First Word: ' + firstWord); 
// console.log('Second Word: ' + secondWord); 
// console.log('Third Word: ' + thirdWord); 
// console.log('Fourth Word: ' + fourthWord); 
// console.log('Fifth Word: ' + fifthWord); 
// console.log('Sixth Word: ' + sixthWord); 
// console.log('Seventh Word: ' + seventhWord); 
// console.log('Eighth Word: ' + eighthWord)

 


//============
// Soal No.3
//============


var sentence2 = 'wow JavaScript is so cool'; 

var firstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4,14);
var thirdWord2 = sentence2.substring(15, 17);
var fourthWord2 = sentence2.substring(18, 20);
var fifthWord2 = sentence2.substring(21);

// console.log('First Word: ' + firstWord2); 
// console.log('Second Word: ' + secondWord2); 
// console.log('Third Word: ' + thirdWord2); 
// console.log('Fourth Word: ' + fourthWord2); 
// console.log('Fifth Word: ' + fifthWord2);






//============
// Soal No.3
//============


var sentence3 = 'wow JavaScript is so cool'; 

var firstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substring(4,14); 
var thirdWord3 = sentence3.substring(15, 17); 
var fourthWord3 = sentence3.substring(18, 20); 
var fifthWord3 = sentence3.substring(21); 

var firstWordLength = firstWord3.length;  
var secondWordLength = secondWord3.length; 
var thirdWordLength = thirdWord3.length; 
var fourthWordLength = fourthWord3.length; 
var fifthWordLength = fifthWord3.length; 

// console.log('First Word: ' + firstWord3 + ', with length: ' + firstWordLength); 
// console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength); 
// console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength); 
// console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength); 
// console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength); 